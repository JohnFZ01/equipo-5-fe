import { createRouter, createWebHistory } from 'vue-router'
import App from './App.vue'

import LogIn from './components/LogIn.vue'
import SignUp from './components/SignUp.vue'
import Home from './components/Home.vue'
import species from './components/species.vue'
import Organizations from './components/Organizations.vue'
import UpdateSpecie from './components/UpdateSpecie.vue'

const routes = [{
  path: '/',
  name: 'root',
  component: App
},
{
  path: '/user/logIn',
  name: "logIn",
  component: LogIn
},
{
  path: '/user/signUp',
  name: "signUp",
  component: SignUp
},
{
  path: '/user/home',
  name: "home",
  component: Home
},

{
  path: '/species',
  name: "species",
  component: species
},

{
  path: '/UpdateSpecies',
  name: "UpdateSpecies",
  component: UpdateSpecie
},
{
  path: '/Organizations',
  name: "Organizations",
  component: Organizations
}



]


const router = createRouter({
  history: createWebHistory(),
  routes
})

export default router